import { StringHelper } from "../helpers";
import { PagingRequestType } from "../types/paging-request.type";
import { get } from "./IndexService";

const url = 'app-settings';

const getSetting = async (request: PagingRequestType): Promise<any> => {
  const qs = StringHelper.toQuerystring(url, request);
  return get(qs);
}

const settingService = {
  getSetting
}

export default settingService;