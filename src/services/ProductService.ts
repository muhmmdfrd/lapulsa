import { StringHelper } from "../helpers";
import { PagingRequestType } from "../types/paging-request.type";
import { get } from "./IndexService";

const url = 'products';

const getproduct = async (request: PagingRequestType): Promise<any> => {
  const qs = StringHelper.toQuerystring(url, request)
  return get(qs);
}

const productService = {
  getproduct
}

export default productService;