import productService from './ProductService';
import transService from './TransService';
import authService from './AuthService';
import settingService from './SettingService';

export {
  productService,
  transService,
  authService,
  settingService
}