import { AuthType } from "../types/auth.type";
import { post } from "./IndexService";

const url = 'users/auth';

const auth = () => {
  return post(url, {
    username: 'sysadmin',
    password: 'admin123'
  });
}

const authAdmin = (auth: AuthType) => {
  return post(url, auth);
}

const authService = {
  auth,
  authAdmin
}

export default authService;