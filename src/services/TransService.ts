import { StringHelper } from "../helpers";
import { AddTransaType } from "../types/add-trans.type";
import { PagingRequestType } from "../types/paging-request.type";
import { get, post, put } from "./IndexService";

const url = 'transactions';

const getLatest = async (): Promise<any> => {
  return get(`${url}/latest`);
}

const getPaged = async (request: PagingRequestType): Promise<any> => {
  const qs = StringHelper.toQuerystring(url, request);
  return get(qs);
}

const getPagedAdmin = async (request: PagingRequestType): Promise<any> => {
  const qs = StringHelper.toQuerystring(url + '/admin', request);
  return get(qs);
}

const addTransaction = async (trans: AddTransaType): Promise<any> => {
  return post(url, trans);
}

const getTransactionById = async (id: number): Promise<any> => {
  return get(`${url}/${id}`);
}

const confirmPayment = async (id: number): Promise<any> => {
  return put(`${url}/payment/${id}`, {});
}

const confirmAdmin = async (id: number): Promise<any> => {
  return put(`${url}/admin/${id}`, {});
}

const paymentSuccess = async (id: number): Promise<any> => {
  return put(`${url}/success/${id}`, {});
}

const getDetail = async (transId: number): Promise<any> => {
  return get(`${url}-detail/${transId}`);
}

const transService = {
  getLatest,
  getPaged,
  getPagedAdmin,
  addTransaction,
  getTransactionById,
  confirmPayment,
  confirmAdmin,
  paymentSuccess,
  getDetail
}

export default transService;