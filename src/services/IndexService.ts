import axios, { AxiosError, AxiosRequestConfig, AxiosResponse } from 'axios';
import { authService } from '.';
import environment from '../environments';
import { ApiResponseType } from '../types/api-response.type';

const client = axios.create({
  baseURL: environment.baseApiUrl,
  timeoutErrorMessage: 'No internet connection.',
  withCredentials: true
});

client.interceptors.request.use(async (config: AxiosRequestConfig<any>) => {
  const key = localStorage.getItem('key');
  if (key != null) {
    // @ts-ignore
    config.headers.Authorization = 'Bearer ' + key;
  }
  config.responseType = 'json';
  // @ts-ignore
  config.headers.common['system-patch-sec-referer'] = 'hF9c6gufCapnvg';
  return config;
})

// http response interceptor
// for handle error message
// from body or http status code
client.interceptors.response.use(async (response: AxiosResponse<ApiResponseType<any>>) => {
  switch (response.data.code) {
    case 1001:
      throw new Error('Unathorized.');
    case 1004:
      throw new Error('Data not found. Please recheck your data.');
    case 9999:
      throw new Error('Something went wrong here.');
    default:
      return response.data.result;
  }
}, async (err: AxiosError) => {
  switch (err.response?.status) {
    case 401:
      const token = await authService.auth();
      localStorage.setItem('key', token);
      axios.defaults.headers.common['Authorization'] = `Bearer ${token}`;
      return client(err.config);
      // throw new Error('Unauthorized.');
    case 400:
      throw new Error('Bad request. ' + err.response?.data.message);
    case undefined:
      throw new Error('Failed connecting to server. Please try again.');
    default:
      throw new Error('Something went wrong here.');
  }
});

const get = async (endpoint: string): Promise<any> => {
  return await client.get(endpoint);
}

const post = async (endpoint: string, data: any): Promise<any> => {
  return await client.post(endpoint, data);
}

const put = async (endpoint: string, data: any): Promise<any> => {
  return await client.put(endpoint, data);
}

const deleted = async (endpoint: string): Promise<any> => {
  return await client.delete(endpoint);
}

export {
  get,
  post,
  put,
  deleted
}