import * as React from 'react';
import { AdminRoute } from './routes';

interface AdminAppProps { }
 
const AdminApp: React.FC<AdminAppProps> = () => {
  return (
    <main className="main-content mt-0">
      <AdminRoute />
    </main>
  )
}
 
export default AdminApp;