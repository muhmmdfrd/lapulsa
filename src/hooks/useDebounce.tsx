import * as React from "react";

const useDebounce = (value: any, delay: number) => {
  const [val, setVal] = React.useState<any>(value);

  React.useEffect(() => {
    const handler = setTimeout(() => setVal(value), delay);
    return () => clearTimeout(handler);
  }, [value, delay]);

  return val;
};

export default useDebounce;
