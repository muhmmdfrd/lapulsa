import * as React from 'react';

const UseLoading = (initValue: boolean) => {
  const [val, setVal] = React.useState<boolean>(initValue);

  const change = (value: boolean) => {
    setTimeout(() => setVal(value), 500);
  }

  const updateValue = React.useRef<any>({
    toggle: () => setVal(!val),
    show: () => setVal(true),
    hide: () => change(false)
  });

  return [val, updateValue.current];
}
 
export default UseLoading;