export type TransactionType = {
  id: number,
  productName: string,
  price: number,
  number: string,
  status: number,
  createdDate: Date
}