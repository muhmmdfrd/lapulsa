export type PagingRequestType = {
  size: number,
  index: number,
  keyword: string
}