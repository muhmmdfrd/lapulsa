export type ApiResponseType<T> = {
  message: string,
  code: number,
  success: boolean,
  result: T
}