export type AddTransaType = {
  productId: number,
  number: string,
  price: number
}