export type SidebarType = {
  name: string,
  icon: string,
  path: string
}