import * as React from 'react';
import { useTranslation } from 'react-i18next';
import { useNavigate } from 'react-router-dom';
import { DatetimeHelper, StringHelper, ToastHelper } from '../../helpers';
import { useDebounce, UseLoading } from '../../hooks';
import { transService } from '../../services';
import { PagingRequestType } from '../../types/paging-request.type';
import Pagination from '../Pagination';
import TableLoader from '../TableLoader';

interface TransactionAdminProps {}

const TransactionTableAdmin: React.FC<TransactionAdminProps> = () => {
  const { t } = useTranslation();
  const [data, setData] = React.useState<any[]>([]);
  const [total, setTotal] = React.useState<number>(0);
  const [isLoading, setLoading] = UseLoading(false);
  const [request, setRequest] = React.useState<PagingRequestType>({
    index: 1,
    size: 8,
    keyword: '',
  });
  const keyword = useDebounce(request.keyword, 500);
  const navigate = useNavigate();

  const getStatus = (type: number) => {
    switch (type) {
      case -2:
        return { status: 'Refund', icon: 'redo', class: 'danger' };
      case -1:
        return { status: 'Failed', icon: 'clear', class: 'danger' };
      case 0:
        return {
          status: 'Waiting For Payment',
          icon: 'attach_money',
          class: 'info',
        };
      case 1:
        return {
          status: 'Waiting For Approval By Admin',
          icon: 'how_to_reg',
          class: 'info',
        };
      case 2:
        return { status: 'Pending', icon: 'pending', class: 'warning' };
      case 3:
        return { status: 'Success', icon: 'check', class: 'success' };
      default:
        return { status: '-', icon: 'user', class: 'success' };
    }
  };

  const setCurrent = (index: number) => {
    setRequest({
      ...request,
      index: index,
    });
  };

  const search = (keyword: string) => {
    setRequest({
      ...request,
      index: 1,
      keyword: keyword,
    });
  };

  React.useEffect(() => {
    setLoading.show();
    transService
      .getPagedAdmin(request)
      .then((response) => {
        setData(response.data);
        setTotal(response.total);
      })
      .catch((err: Error) => {
        ToastHelper.error(err.message);
      })
      .finally(() => setLoading.hide());
  }, [request, setLoading, keyword]);

  return (
    <>
      <div className="card my-4">
        <div className="card-header p-0 position-relative mt-n4 mx-3 z-index-2">
          <div className="bg-gradient-primary shadow-primary border-radius-lg pt-4 pb-3">
            <h6 className="text-white text-capitalize ps-3">
              {t('product.product')}
            </h6>
          </div>
          <div className="row mt-5">
            <div className="col-md-3">
              <div className="input-group input-group-outline">
                <input
                  autoFocus
                  type="text"
                  className="form-control"
                  placeholder={t('product.placeholder_keyword')}
                  value={request.keyword}
                  onChange={(e) => search(e.target.value)}
                />
              </div>
            </div>
          </div>
        </div>
        <div className="card-body px-0 pb-2">
          <div className="table-responsive p-0">
            <table className="table align-items-center mb-0">
              <thead>
                <tr>
                  <th className="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
                    {t('transaction.product_name')}
                  </th>
                  <th className="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
                    {t('transaction.number')}
                  </th>
                  <th className="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">
                    {t('transaction.price')}
                  </th>
                  <th className="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
                    {t('transaction.date')}
                  </th>
                  <th className="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
                    {t('transaction.status')}
                  </th>
                  <th className="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
                    {t('transaction.status')}
                  </th>
                </tr>
              </thead>
              <tbody>
                {data.map((v) => {
                  return (
                    <tr key={v.id}>
                      <td>
                        <div className="d-flex px-2 py-1">
                          <div className="d-flex flex-column justify-content-center">
                            <h6 className="mb-0 text-sm">{v.productName}</h6>
                          </div>
                        </div>
                      </td>
                      <td>
                        <h6 className="text-sm font-weight-bold mb-0">
                          {v.number}
                        </h6>
                      </td>
                      <td>
                        <h6 className="text-sm font-weight-bold mb-0">
                          {StringHelper.toDollar(v.price)}
                        </h6>
                      </td>
                      <td>
                        <h6 className="text-sm font-weight-bold mb-0">
                          {DatetimeHelper.format(v.createdDate)}
                        </h6>
                      </td>
                      <td>
                        <h6
                          className={`text-sm font-weight-bold mb-0 text-${
                            getStatus(v.status).class
                          }`}
                        >
                          {getStatus(v.status).status}
                        </h6>
                      </td>
                      <td className="text-sm" valign="middle">
                        <button
                          className={`btn badge badge-sm bg-gradient-secondary`}
                          onClick={() => navigate(`${v.id}`)}
                        >
                          Detail
                        </button>
                      </td>
                    </tr>
                  );
                })}
              </tbody>
            </table>
          </div>
        </div>
        <Pagination
          current={request.index}
          setCurrent={setCurrent}
          sizePerPage={request.size}
          totalRecord={total}
        />
      </div>
      {isLoading && <TableLoader />}
    </>
  );
};

export default TransactionTableAdmin;
