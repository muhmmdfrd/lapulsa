import * as React from 'react';
import { useTranslation } from 'react-i18next';
import { Pagination, TableLoader } from '..';
import { StringHelper, ToastHelper } from '../../helpers';
import { useDebounce, UseLoading } from '../../hooks';
import { productService } from '../../services';
import { PagingRequestType } from '../../types/paging-request.type';

interface ProductTableAdminProps {}

const ProductTableAdmin: React.FC<ProductTableAdminProps> = () => {
  const { t } = useTranslation();
  const [data, setData] = React.useState<any[]>([]);
  const [total, setTotal] = React.useState<number>(0);
  const [isLoading, setLoading] = UseLoading(false);
  const [request, setRequest] = React.useState<PagingRequestType>({
    index: 1,
    size: 8,
    keyword: '',
  });
  const keyword = useDebounce(request.keyword, 500);

  const setCurrent = (index: number) => {
    setRequest({
      ...request,
      index: index,
    });
  };

  const search = (keyword: string) => {
    setRequest({
      ...request,
      index: 1,
      keyword: keyword,
    });
  };

  React.useEffect(() => {
    setLoading.show();
    productService
      .getproduct(request)
      .then((response) => {
        setData(response.data);
        setTotal(response.total);
      })
      .catch((err: Error) => {
        ToastHelper.error(err.message);
      })
      .finally(() => setLoading.hide());
  }, [request, keyword, setLoading]);

  return (
    <>
      <div className="card my-4">
        <div className="card-header p-0 position-relative mt-n4 mx-3 z-index-2">
          <div className="bg-gradient-primary shadow-primary border-radius-lg pt-4 pb-3">
            <h6 className="text-white text-capitalize ps-3">
              {t('product.product')}
            </h6>
          </div>
          <div className="row mt-5">
            <div className="col-md-3">
              <div className="input-group input-group-outline">
                <input
                  autoFocus
                  type="text"
                  className="form-control"
                  placeholder={t('product.placeholder_keyword')}
                  value={request.keyword}
                  onChange={(e) => search(e.target.value)}
                />
              </div>
            </div>
          </div>
        </div>
        <div className="card-body px-0 pb-2">
          <div className="table-responsive p-0">
            <table className="table align-items-center mb-0">
              <thead>
                <tr>
                  <th className="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
                    {t('product.name')}
                  </th>
                  <th className="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">
                    {t('product.price')}
                  </th>
                  <th className="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
                    {t('product.status')}
                  </th>
                </tr>
              </thead>
              <tbody>
                {data.map((v) => {
                  return (
                    <tr key={v.id}>
                      <td>
                        <div className="d-flex px-2 py-1">
                          <div className="d-flex flex-column justify-content-center">
                            <h6 className="mb-0 text-sm">{v.name}</h6>
                          </div>
                        </div>
                      </td>
                      <td>
                        <p className="text-sm font-weight-bold mb-0">
                          {StringHelper.toDollar(v.price)}
                        </p>
                      </td>
                      <td className="text-sm">
                        <span
                          className={`badge badge-sm bg-gradient-${
                            v.status ? 'success' : 'danger'
                          }`}
                        >
                          {t(`product.${v.status.toString()}`)}
                        </span>
                      </td>
                    </tr>
                  );
                })}
              </tbody>
            </table>
          </div>
        </div>
        <Pagination
          current={request.index}
          setCurrent={setCurrent}
          sizePerPage={request.size}
          totalRecord={total}
        />
      </div>
      {isLoading && <TableLoader />}
    </>
  );
};

export default ProductTableAdmin;
