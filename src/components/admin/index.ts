import SidebarAdmin from './SidebarAdmin';
import ProductTableAdmin from './ProductTableAdmin';
import SettingTableAdmin from './SettingTableAdmin';
import TransactionTableAdmin from './TransactionTableAdmin';

export {
  SidebarAdmin,
  ProductTableAdmin,
  SettingTableAdmin,
  TransactionTableAdmin
}