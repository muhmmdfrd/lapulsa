import * as React from 'react';
import { Link, NavLink } from 'react-router-dom';
import { Logo } from '../../assets';
import { SidebarType } from '../../types/sidebar.type';

interface SidebarAdminProps { }
 
const SidebarAdmin: React.FC<SidebarAdminProps> = () => {
  const activeClass = 'nav-link text-white active bg-gradient-primary';
  const inactiveClass = 'nav-link text-white';
  const title = 'Lapulsa.com'.toUpperCase();

  const menus: SidebarType[] = [
    {
      name: 'Dashboard',
      icon: 'dashboard',
      path: 'dashboard'
    },
    {
      name: 'Products',
      icon: 'table_view',
      path: 'product'
    },
    {
      name: 'Transactions',
      icon: 'receipt_long',
      path: 'transaction'
    },
    {
      name: 'Settings',
      icon: 'settings',
      path: 'settings'
    }
  ]
  
  return (
    <aside
    className="sidenav navbar navbar-vertical navbar-expand-xs border-0 border-radius-xl my-3 fixed-start ms-3 bg-gradient-dark"
    id="sidenav-main"
  >
    <div className="sidenav-header">
      <i
        className="fas fa-times p-3 cursor-pointer text-white opacity-5 position-absolute end-0 top-0 d-none d-xl-none"
        aria-hidden="true"
        id="iconSidenav"
      />
      <Link to={'/'} className="navbar-brand m-0">
        <img src={Logo} className="navbar-brand-img h-100" alt="main_logo" />
        <span className="ms-1 font-weight-bold text-white">&nbsp;{title}</span>
      </Link>
    </div>
    <hr className="horizontal light mt-0 mb-2" />
    <div
      className="collapse navbar-collapse w-auto max-height-vh-100"
      id="sidenav-collapse-main"
    >
      <ul className="navbar-nav">
        {
          menus.map((v) => {
            return (
              <li key={v.name} className="nav-item">
                <NavLink className={({ isActive }) => isActive ? activeClass : inactiveClass} to={v.path}>
                  <div className="text-white text-center me-2 d-flex align-items-center justify-content-center">
                    <i className="material-icons opacity-10">{v.icon}</i>
                  </div>
                  <span className="nav-link-text ms-1">{v.name}</span>
                </NavLink>
              </li>
            );
          })
        }
      </ul>
    </div>
  </aside>
  );
}
 
export default SidebarAdmin;