import * as React from 'react';
import { useParams } from 'react-router-dom';
import { DatetimeHelper, ToastHelper } from '../../../helpers';
import { transService } from '../../../services';
import { TransactionType } from '../../../types/transaction.type';
// @ts-ignore
import { ShimmerCategoryList } from 'react-shimmer-effects';
import { Link } from 'react-router-dom';

interface TransactionDetailCardProps {
  setStatus: any;
  status: number;
}

const TransactionDetailCard: React.FC<TransactionDetailCardProps> = ({
  setStatus,
  status,
}) => {
  const params = useParams();
  const [isLoading, setLoading] = React.useState<boolean>(false);
  const [data, setData] = React.useState<TransactionType>({
    createdDate: new Date(),
    id: 0,
    number: '',
    price: 0,
    productName: '',
    status: -999,
  });

  const getStatus = (type: number) => {
    switch (type) {
      case -2:
        return { status: 'Refund', class: 'danger' };
      case -1:
        return { status: 'Failed', class: 'danger' };
      case 0:
        return { status: 'Waiting For Payment', class: 'info' };
      case 1:
        return { status: 'Waiting For Approval By Admin', class: 'info' };
      case 2:
        return { status: 'In Progress By Admin', class: 'warning' };
      case 3:
        return { status: 'Success', class: 'success' };
      default:
        return { status: '-', class: 'success' };
    }
  };

  const fetchData = () => {
    setLoading(true);
    transService
      .getTransactionById(Number(params.id))
      .then((response) => setData(response))
      .catch((err: Error) => ToastHelper.error(err.message))
      .finally(() => setTimeout(() => setLoading(false), 200));
  };

  const confirmPayment = () => {
    transService
      .confirmPayment(Number(params.id))
      .then(() =>
        ToastHelper.success(
          'Data berhasil dikonfirmasi, silakan tunggu proses pesanan Anda'
        )
      )
      .catch((err: Error) => ToastHelper.error(err.message))
      .finally(() => setStatus(status + 1));
  };

  React.useEffect(() => {
    fetchData();

    return () =>
      setData({
        createdDate: new Date(),
        id: 0,
        number: '',
        price: 0,
        productName: '',
        status: -999,
      });
  }, [status]);

  return isLoading ? (
    <ShimmerCategoryList title items={3} />
  ) : (
    <div className="card h-100 mb-4">
      <div className="card-header pb-0 px-3">
        <div className="row">
          <div className="col-md-6">
            <Link to={'/transaction'}>
              <h6 className="mb-0">
                <i className="material-icons text-lg">{'west'}</i>&nbsp;&nbsp;
                {'Detail Transaksi'}
              </h6>
            </Link>
          </div>
        </div>
      </div>
      <div className="card-body pt-4 p-3">
        <ul className="list-group">
          <TransactionStatus
            title={'Produk'}
            value={data.productName}
            icon={'shopping_cart'}
          />
          <TransactionStatus
            title={'Nomor'}
            value={data.number}
            icon={'smartphone'}
          />
          <TransactionStatus
            title={'Tanggal'}
            value={DatetimeHelper.format(data.createdDate)}
            icon={'event'}
          />
          <TransactionStatus
            title={'Status'}
            value={getStatus(data.status).status}
            icon={'label'}
            color={getStatus(data.status).class}
          />
          <TransactionStatus
            title={'Harga'}
            value={`${data.price}`}
            icon={'attach_money'}
            isShowCopy
            isCurrency
          />
          <TransactionStatus
            title={'Bayar ke'}
            value={'lapulsa@outlook.co.id'}
            icon={'email'}
            color={'success'}
            isShowCopy
          />
          <li className="list-group-item border-0 d-flex justify-content-between ps-0 border-radius-lg">
            <p className="text-sm">
              (P.S) Harap untuk teliti, pilih metode bayar sebagai teman &
              keluarga. Lakukan Konfirmasi setelah melakukan pembayaran
            </p>
          </li>
        </ul>
        {data.status === 0 && (
          <div className="row mx-1 mt-4">
            <button
              type="button"
              className="btn btn-block btn-primary"
              onClick={() => confirmPayment()}
            >
              Saya Sudah Bayar
            </button>
          </div>
        )}
      </div>
    </div>
  );
};

interface TransactionStatusProps {
  title: string;
  value: string;
  icon: string;
  color?: string;
  isShowCopy?: boolean;
  isCurrency?: boolean;
}

const TransactionStatus: React.FC<TransactionStatusProps> = ({
  value,
  title,
  icon,
  color = 'primary',
  isShowCopy = false,
  isCurrency = false,
}) => {
  const onCopy = () => {
    navigator.clipboard.writeText(value);
    ToastHelper.success('Berhasil copy.');
  };

  return (
    <li className="list-group-item border-0 d-flex justify-content-between ps-0 mb-2 border-radius-lg">
      <div className="d-flex align-items-center">
        <button className="btn btn-icon-only btn-rounded btn-outline-dark mb-0 me-3 p-3 btn-sm d-flex align-items-center justify-content-center">
          <i className="material-icons text-lg">{icon}</i>
        </button>
        <div className="d-flex flex-column">
          <span className="text-md text-dark">{title}</span>
        </div>
      </div>
      <div
        className={`d-flex align-items-center font-weight-bold text-${color} text-md`}
      >
        {isCurrency ? `$ ${value}` : value}
      </div>
      {isShowCopy && (
        <button className="btn btn-sm btn-primary" onClick={() => onCopy()}>
          copy
        </button>
      )}
    </li>
  );
};

export default TransactionDetailCard;
