import * as React from 'react';
import { Link, useParams } from 'react-router-dom';
import { DatetimeHelper, ToastHelper } from '../../../helpers';
import { transService } from '../../../services';
// @ts-ignore
import { ShimmerCategoryList } from "react-shimmer-effects";

interface InformationCardProps { 
  status: number
}
 
const InformationCard: React.FC<InformationCardProps> = ({ status }) => {
  const params = useParams();
  const [data, setData] = React.useState<any[]>([]);
  const [isLoading, setLoading] = React.useState<boolean>(false);

  const fetchData = () => {
    setLoading(true);
    transService.getDetail(Number(params.id))
      .then((response) => setData(response))
      .catch((err: Error) => ToastHelper.error(err.message))
      .finally(() => setLoading(false));
  }

  React.useEffect(() => {
    fetchData();
  }, [status]);

  return isLoading ? <ShimmerCategoryList title items={3} /> : (
    <div className="card h-100">
      <div className="card-header pb-0">
        <Link to={'/transaction'}>
          <h6 className="mb-0">
            <i className="material-icons text-lg">{'west'}</i>&nbsp;&nbsp;
            {'Informasi Status'}
          </h6>
        </Link>
      </div>
      <div className="card-body p-3">
        <div className="timeline timeline-one-side">
          <div className="timeline-block mb-3">
            <span className="timeline-step">
              <i className="material-icons text-info text-gradient">how_to_reg</i>
            </span>
            {
              data.map((v) => <TimeLine key={v.id} date={v.date} status={v.status} />)
            }
          </div>
        </div>
      </div>
    </div>
  );
}

interface TimeLineProps { 
  date: Date,
  status: number
}

const TimeLine: React.FC<TimeLineProps> = ({ date, status }) => {
  const getStatus = (type: number) => {
    switch (type) {
      case -2:
        return { status: 'Refund', icon: 'redo', class: 'danger' };
      case -1:
        return { status: 'Failed', icon: 'clear', class: 'danger' };
      case 0:
        return { status: 'Waiting For Payment', icon: 'attach_money', class: 'info' };
      case 1:
        return { status: 'Waiting For Approval By Admin', icon: 'how_to_reg', class: 'info' };
      case 2:
        return { status: 'In Progress By Admin', icon: 'pending', class: 'warning' };
      case 3:
        return { status: 'Success', icon: 'check', class: 'success' };
      default:
        return { status: 'Waiting For Payment', icon: 'attach_money', class: 'success' };
    }
  }

  return (
    <div className="timeline-block mb-3">
      <span className="timeline-step">
        <i className={`material-icons text-${getStatus(status).class} text-gradient`}>{getStatus(status).icon}</i>
      </span>
      <div className="timeline-content">
        <h6 className="text-dark text-md font-weight-bold mb-0">
          {getStatus(status).status}
        </h6>
        <p className="text-secondary font-weight-bold text-sm mt-1 mb-0">
          {DatetimeHelper.format(date)}
        </p>
      </div>
    </div>
  )
}
 
export default InformationCard;