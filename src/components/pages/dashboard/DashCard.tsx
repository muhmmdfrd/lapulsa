import * as React from 'react';
import { StringHelper } from '../../../helpers';
import './DashCard.css';

interface DashCardProps { 
  icon: string,
  name: string,
  value: any,
  schema: 'dark' | 'primary' | 'success' | 'info' | 'danger'
  type?: 'finance' | 'number'
}
 
const DashCard: React.FC<DashCardProps> = ({ icon, name, value, schema, type = 'number' }) => {
  return (
    <div className="col-xl-3 col-sm-6 mb-xl-0 mb-4">
      <div className="card">
        <div className="card-header p-3 pt-2">
          <div className={`icon icon-lg icon-shape bg-gradient-${schema} shadow-dark text-center border-radius-xl mt-n4 position-absolute`}>
            <i className="material-icons opacity-10">{icon}</i>
          </div>
          <div className="text-end pt-1">
            <p className="text-sm mb-0">{name}</p>
            <h4 className="mb-0">{type === 'finance' ? 'Rp' : ''} {StringHelper.toCurrency(value)}&nbsp;</h4>
          </div>
        </div>
        <hr className="dark horizontal my-0" />
        <div className="card-footer p-3">
          <p className="mb-0">
            <span className="text-success text-sm font-weight-bolder">+55%&nbsp;</span>than lask week
          </p>
        </div>
      </div>
    </div>
  );
}
 
export default DashCard;