import * as React from 'react';
import { useTranslation } from 'react-i18next';
import { DatetimeHelper, ToastHelper } from '../../../helpers';
import { transService } from '../../../services';
// @ts-ignore
import { ShimmerCategoryList } from "react-shimmer-effects";

interface TransactionCardProps { }
 
const TransactionCard: React.FC<TransactionCardProps> = () => {
  const { t } = useTranslation();
  const { dateNow, monthNow, yearNow } = DatetimeHelper;
  const [isLoading, setLoading] = React.useState<boolean>(false);
  const [data, setData] = React.useState<any[]>([]);

  const fetchLatestData = () => {
    setLoading(true);
    transService.getLatest()
      .then((response) => setData(response))
      .catch((err: Error) => ToastHelper.error(err.message))
      .finally(() => setLoading(false));
  }

  React.useEffect(() => {
    fetchLatestData();
  }, []);

  return isLoading ? <ShimmerCategoryList title items={4} /> : (
    <div className="card h-100 mb-4">
      <div className="card-header pb-0 px-3">
        <div className="row">
          <div className="col-md-6">
            <h6 className="mb-0">{t('transaction_card.transaction_today')}</h6>
          </div>
          <div
            className="col-md-6 d-flex justify-content-start justify-content-md-end align-items-center"
          >
            <i className="material-icons me-2 text-lg">date_range</i>
            <small>{`${dateNow()} ${t(`month.${monthNow()}`)} ${yearNow()}`}</small>
          </div>
        </div>
      </div>
      <div className="card-body pt-4 p-3">
        <ul className="list-group">
          {
            data.map((v) => <TransactionStatus key={v.id} status={v.status} productName={v.productName} date={v.createdDate} />)
          }
        </ul>
      </div>
    </div>
  );
}

interface TransactionStatusProps {
  status: number,
  productName: string,
  date: string
}

const TransactionStatus: React.FC<TransactionStatusProps> = ({ status, productName, date }) => {

  const getStatus = (type: number) => {
    switch (type) {
      case -2:
        return { status: 'Refund', icon: 'redo', class: 'danger' };
      case -1:
        return { status: 'Failed', icon: 'clear', class: 'danger' };
      case 0:
        return { status: 'Waiting For Payment', icon: 'attach_money', class: 'info' };
      case 1:
        return { status: 'Waiting For Approval By Admin', icon: 'how_to_reg', class: 'info' };
      case 2:
        return { status: 'In Progress By Admin', icon: 'pending', class: 'warning' };
      case 3:
        return { status: 'Success', icon: 'check', class: 'success' };
      default:
        return { status: '-', icon: 'user', class: 'success' };
    }
  }

  return (
    <li className="list-group-item border-0 d-flex justify-content-between ps-0 mb-2 border-radius-lg">
      <div className="d-flex align-items-center">
        <button
          className={`btn btn-icon-only btn-rounded btn-outline-${getStatus(status).class} mb-0 me-3 p-3 btn-sm d-flex align-items-center justify-content-center`}
        >
          <i className="material-icons text-lg">{getStatus(status).icon}</i>
        </button>
        <div className="d-flex flex-column">
          <h6 className="mb-1 text-dark text-sm">{productName}</h6>
          <span className="text-xs">{DatetimeHelper.format(date)}</span>
        </div>
      </div>
      <div className={`d-flex align-items-center text-${getStatus(status).class} text-gradient text-sm font-weight-bold`}>
        {getStatus(status).status}
      </div>
    </li>
  )
}

export default TransactionCard;