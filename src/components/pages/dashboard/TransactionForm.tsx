import * as React from 'react';
import { useTranslation } from 'react-i18next';
import { useNavigate } from 'react-router-dom';
import Select from 'react-select';
import { ToastHelper } from '../../../helpers';
import { productService, transService } from '../../../services';
import { AddTransaType } from '../../../types/add-trans.type';
import { PagingRequestType } from '../../../types/paging-request.type';

interface TransactionFormProps {}

const TransactionForm: React.FC<TransactionFormProps> = () => {
  const { t } = useTranslation();
  const navigate = useNavigate();
  const [isLoading, setLoading] = React.useState<boolean>(false);
  const [options, setOption] = React.useState<any[]>([]);
  const [form, setForm] = React.useState<AddTransaType>({
    number: '',
    productId: 0,
    price: 0,
  });
  const [request, setRequest] = React.useState<PagingRequestType>({
    index: 1,
    size: 8,
    keyword: '',
  });

  const getProduct = () => {
    setLoading(true);
    productService
      .getproduct(request)
      .then((response: { data: any[] }) => {
        let o: any[] = [];
        response.data.forEach((v) => {
          o.push({
            label: v.name,
            value: { productId: v.id, price: v.price },
          });
          setOption(o);
        });
      })
      .catch((err: Error) => ToastHelper.error(err.message))
      .finally(() => setLoading(false));
  };

  const setOptionValue = (e: any) => {
    setForm({
      ...form,
      productId: e.value.productId,
      price: e.value.price,
    });
  };

  const onInputNumberChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    event.target.value = event.target.value
      .replace(/[^0-9]/g, '')
      .toLowerCase();
    setForm({ ...form, number: event.target.value });
  };

  const onSubmit = (event: React.MouseEvent<any, any>) => {
    event.preventDefault();
    setLoading(true);
    localStorage.setItem('n', form.number);
    transService
      .addTransaction(form)
      .then((id) => navigate(`transaction/${id}`))
      .catch((err: Error) => ToastHelper.error(err.message))
      .finally(() => setLoading(false));
  };

  React.useEffect(() => {
    getProduct();
  }, [request.keyword]);

  return (
    <div className="card h-100 mb-4">
      <div className="card-header pb-0 px-3">
        <div className="row">
          <div className="col-md-6">
            <h6 className="mb-0">{t('transaction_form.title')}</h6>
          </div>
        </div>
      </div>
      <div className="card-body pt-4 p-3">
        <form className="form text-start">
          <div className="input-group input-group-outline my-3">
            <input
              type={'text'}
              className="form-control"
              placeholder={t('transaction_form.placeholder_number')}
              onChange={(e) => onInputNumberChange(e)}
            />
          </div>
          <Select
            placeholder={t('transaction_form.choose_service')}
            className="form-control"
            options={options}
            isLoading={isLoading}
            onInputChange={(e) => setRequest({ ...request, keyword: e })}
            onChange={(e) => setOptionValue(e)}
          />
          <div className="input-group input-group-outline my-3">
            <button
              type={'submit'}
              style={{ zIndex: 0 }}
              className="btn btn-sm btn-info"
              onClick={(event) => onSubmit(event)}
              disabled={form.number === '' || form.productId === 0}
            >
              {t('transaction_form.pay')}
            </button>
          </div>
        </form>
      </div>
    </div>
  );
};

export default TransactionForm;
