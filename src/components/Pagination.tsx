import * as React from 'react';

interface PaginationProps { 
  maxVisible?: number,
  current: number,
  totalRecord: number,
  setCurrent: any,
  sizePerPage: number
}
 
const Pagination: React.FC<PaginationProps> = ({ maxVisible = 2, current, totalRecord, setCurrent, sizePerPage }) => {
  const [totalPage, setTotalPage] = React.useState<number>(1);

  React.useEffect(() => {
    setTotalPage(Math.ceil(totalRecord / sizePerPage));
  }, [totalRecord, current]);

  const startPage = (): number => {
    switch (current) {
      case 1:
        return 1;
      case totalPage:
        return totalPage - maxVisible;
      default:
        return current - 1;
    }
  }
  
  const pages = (): any[] => {
    const range = [];
    const all = startPage() + maxVisible;
    const minimum = totalPage;
    const limit = Math.min(all, minimum);

    for (let i = startPage(); i <= limit; i++) {
      if (i === 0) continue;

      range.push({
        name: i,
        isDisable: i === current
      });
    }
    return range;
  }

  const isInFirstPage = () => current === 1;
  const isInLastPage = () => current === totalPage;
  const prevStatus = () => isInFirstPage() ? 'disabled' : '';
  const nextStatus = () => isInLastPage() ? 'disabled': '';
  const isPageActive = (index: number) => current === index ? 'active' : '';

  return (
    <nav className="mt-3" aria-label="page navigation example">
      <ul className="pagination justify-content-center">
        <li className={`page-item ${prevStatus()}`}>
          <button className="page-link"aria-label="Previous" onClick={() => setCurrent(1)}>
            <span className="material-icons">
              first_page
            </span>
            <span className="sr-only">First</span>
          </button>
        </li>
        <li className={`page-item ${prevStatus()}`}>
          <button className="page-link" aria-label="Previous" onClick={() => setCurrent(current - 1)}>
            <span className="material-icons">
              chevron_left
            </span>
            <span className="sr-only">Prev</span>
          </button>
        </li>
        {
          pages().map((v) => 
            <li key={v.name} className={`page-item ${isPageActive(v.name)}`}>
              <a className="page-link" onClick={() => setCurrent(v.name)} href="javascript:;">{v.name}</a>
            </li>)
        }
        <li className={`page-item ${nextStatus()}`}>
          <button className="page-link" aria-label="Next" onClick={() => setCurrent(current + 1)}>
            <span className="material-icons">
              chevron_right
            </span>
            <span className="sr-only">Next</span>
          </button>
        </li>
        <li className={`page-item ${nextStatus()}`}>
          <button className="page-link" aria-label="Next" onClick={() => setCurrent(totalPage)}>
            <span className="material-icons">
              last_page
            </span>
            <span className="sr-only">Last</span>
          </button>
        </li>
      </ul>
    </nav>
  );
}
 
export default Pagination;