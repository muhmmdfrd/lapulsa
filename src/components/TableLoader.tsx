import * as React from 'react';

interface TableLoaderProps {
  
}
 
const TableLoader: React.FC<TableLoaderProps> = () => {
  return (
    <div className="text-center" style={{ position: 'fixed', top: 0, left: 0, zIndex: 10, width: '100%', height: '100%' }}>
      <div className="spinner-border text-primary" style={{marginTop: 25 + '%'}} role="status">
        <span className="sr-only">Loading...</span>
      </div>
    </div>
  );
}
 
export default TableLoader;