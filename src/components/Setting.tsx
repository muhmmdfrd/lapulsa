import * as React from 'react';

interface SettingProps { }
 
const Setting: React.FC<SettingProps> = () => {  
  
  const setDarkMode = (event: any): void => {
    const e = event as React.ChangeEvent<HTMLInputElement>;
    const init = 'g-sidenav-show bg-gray-200';
    document.body.className = e.target.checked ? `${init} dark-version` : init;
  }

  return (
    <div className="fixed-plugin">
      <a className="fixed-plugin-button text-dark position-fixed px-3 py-2" href="#/">
        <i className="material-icons py-2">settings</i>
      </a>
      <div className="card shadow-lg">
        <div className="card-header pb-0 pt-3">
          <div className="float-start">
            <h5 className="mt-3 mb-0">Setting</h5>
          </div>
          <div className="float-end mt-4">
            <button className="btn btn-link text-dark p-0 fixed-plugin-close-button">
              <i className="material-icons">clear</i>
            </button>
          </div>
        </div>
        <div className="card-body pt-sm-3 pt-0">
          <hr className="horizontal dark my-1" />
          <div className="mt-2 d-flex">
            <h6 className="mb-0">Dark Mode</h6>
            <div className="form-check form-switch ps-0 ms-auto my-auto">
              <input
                className="form-check-input mt-1 ms-auto"
                type="checkbox"
                id="dark-version"
                onClick={(event) => setDarkMode(event)}
              />
            </div>
          </div>
          <hr className="horizontal dark my-sm-4" />
        </div>
      </div>
    </div>
  );
}
 
export default Setting;