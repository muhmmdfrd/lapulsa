import Sidebar from './Sidebar';
import Navbar from './Navbar';
import Setting from './Setting';
import Pagination from './Pagination';
import TableLoader from './TableLoader';

export * from './pages';
export * from './admin';

export {
  Sidebar,
  Navbar,
  Setting,
  Pagination,
  TableLoader
};