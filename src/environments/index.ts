const currentDev = import.meta.env;

const devUrl = 'http://127.0.0.1:8080';
const prodUrl = 'https://24be-110-137-194-78.ap.ngrok.io';

const development = {
  mode: currentDev,
  baseUrl: devUrl,
  baseApiUrl: `${devUrl}/api/v1/`,
  signalrUrl: `${devUrl}/signalr`,
};

const production = {
  mode: currentDev,
  baseUrl: prodUrl,
  baseApiUrl: `${prodUrl}/api/v1/`,
  signalrUrl: `${prodUrl}/signalr`,
};

if (currentDev.DEV) {
  console.log = () => {};
  console.info = () => {};
  console.error = () => {};
  console.debug = () => {};
}

const mode = currentDev.DEV ? development : production;
const environment = { ...mode };

export default environment;
