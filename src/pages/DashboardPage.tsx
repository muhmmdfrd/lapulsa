import * as React from 'react';
import { useTranslation } from 'react-i18next';
import { DashCard, TransactionCard, TransactionForm } from '../components';

interface DashboardPageProps { }
 
const DashboardPage: React.FC<DashboardPageProps> = () => {
  const { t } = useTranslation();

  return (
    <div className="container-fluid py-4">
      <div className="row">
        <DashCard name={t('dashboard.balance')} icon={'paid'} value={13891230} schema={'dark'} type={'finance'} />
        <DashCard name={t('dashboard.transaction_today')} icon={'person'} value={2300} schema={'primary'} />
        <DashCard name={t('dashboard.curse_today')} icon={'shopping_cart'} value={'$1 = Rp 14.226'} schema={'info'} />
        <DashCard name={t('dashboard.product')} icon={'summarize'} value={341} schema={'success'} />
      </div>

      <div className="row">
        <div className="col-md-7 mt-4">
         <TransactionForm />
        </div>
        <div className="col-md-5 mt-4">
          <TransactionCard />
        </div>
      </div>
    </div>
  );
}
 
export default DashboardPage;