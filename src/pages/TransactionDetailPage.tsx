import * as React from 'react';
import { useParams } from 'react-router-dom';
import { InformationCard, TransactionDetailCard } from '../components';

interface TransactionDetailPageProps {}

const TransactionDetailPage: React.FC<TransactionDetailPageProps> = () => {
  const params = useParams();
  const client = new WebSocket(
    `ws://127.0.0.1:8080/api/v1/ws?username=${params.id}`
  );
  const [status, setStatus] = React.useState<number>(0);

  React.useEffect(() => {
    client.onmessage = (response) => {
      const data = JSON.parse(response.data);
      console.log(data.Message);
      console.log(status);
      // if (data.Message === 'success') {
      setStatus((s) => s + 1);
      // }
    };

    // return () => client.close()
  }, []);

  return (
    <div className="container-fluid py-4">
      <div className="row">
        <div className="col-md-6">
          <InformationCard status={status} />
        </div>
        <div className="col-md-6">
          <TransactionDetailCard status={status} setStatus={setStatus} />
        </div>
      </div>
    </div>
  );
};

export default TransactionDetailPage;
