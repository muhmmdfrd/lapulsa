import * as React from 'react';
import { ToastHelper } from '../helpers';
import { authService, transService } from '../services';
import { AuthType } from '../types/auth.type';

interface LoginPageProps { }
 
const LoginPage: React.FC<LoginPageProps> = () => {
  const [form, setForm] = React.useState<AuthType>({
    username: '',
    password: ''
  });
  const client = new WebSocket('ws://127.0.0.1:8080/api/v1/ws-admin?to=29');

  const send = () => {
    client.send(JSON.stringify({
      Message: 'success'
    }))
  }

  const admin = () => {
    transService.confirmAdmin(29)
      .then(() => send())
      .catch((err: Error) => ToastHelper.error(err.message));
  }

  const success = () => {
    transService.paymentSuccess(29)
      .catch(() => send())
      .catch((err: Error) => ToastHelper.error(err.message));
  }

  React.useEffect(() => {
    client.onopen = () => ToastHelper.success('Connected.');

    // return () => client.close();
  }, []);

  const auth = () => 
    ToastHelper.promise(authService.authAdmin(form), "Loading...", "Failed", "Success");

  return (
    <div className="page-header align-items-start min-vh-100" style={{backgroundImage: 'url("https://images.unsplash.com/photo-1497294815431-9365093b7331?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1950&q=80")'}}>
      <span className="mask bg-gradient-dark opacity-6" />
      <div className="container my-auto">
        <div className="row">
          <div className="col-lg-4 col-md-8 col-12 mx-auto">
            <div className="card z-index-0 fadeIn3 fadeInBottom">
              <div className="card-header p-0 position-relative mt-n4 mx-3 z-index-2">
                <div className="bg-gradient-primary shadow-primary border-radius-lg py-3 pe-1">
                  <h4 className="text-white font-weight-bolder text-center mt-2 mb-0">
                    Sign in
                  </h4>
                </div>
              </div>
              <div className="card-body">
                <form className="text-start">
                  <div className="input-group input-group-outline my-3">
                    <label className="form-label">Username</label>
                    <input type="text" className="form-control" onChange={(e) => setForm({...form, username: e.target.value})} />
                  </div>
                  <div className="input-group input-group-outline mb-3">
                    <label className="form-label">Password</label>
                    <input type="password" className="form-control" onChange={(e) => setForm({...form, password: e.target.value})} />
                  </div>
                  <div className="text-center">
                    <button
                      onClick={(e) => {
                        e.preventDefault();
                        send();
                        // success();
                      }}
                      type="submit"
                      className="btn bg-gradient-primary w-100 my-4 mb-2"
                    >
                      Sign in
                    </button>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
 
export default LoginPage;