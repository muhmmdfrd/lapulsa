import * as React from 'react';
import { TransactionTable } from '../components';

interface TransactionPageProps { }
 
const TransactionPage: React.FC<TransactionPageProps> = () => {
  return (
    <div className="container-fluid py-4">
      <div className="row">
        <div className="col-md-12">
          <TransactionTable />
        </div>
      </div>
    </div>
  );
}
 
export default TransactionPage;