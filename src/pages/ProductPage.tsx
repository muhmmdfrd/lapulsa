import * as React from 'react';
import { ProductTable } from '../components';

interface ProductPageProps { }
 
const ProductPage: React.FC<ProductPageProps> = () => {
  return (
    <div className="container-fluid py-4">
      <div className="row">
        <div className="col-md-12">
          <ProductTable />
        </div>
      </div>
    </div>
  );
}
 
export default ProductPage;