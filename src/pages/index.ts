import DashboardPage from './DashboardPage';
import ProductPage from './ProductPage';
import TransactionPage from './TransactionPage';
import LoginPage from './LoginPage';
import TransactionDetailPage from './TransactionDetailPage';

export {
  DashboardPage,
  ProductPage,
  TransactionPage,
  LoginPage,
  TransactionDetailPage
};