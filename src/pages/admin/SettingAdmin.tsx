import * as React from 'react';
import { SettingTableAdmin } from '../../components';

interface SettingAdminProps { }
 
const SettingAdmin: React.FC<SettingAdminProps> = () => {
  return (
    <div className="container-fluid py-4">
    <div className="row">
      <div className="col-md-12">
        <SettingTableAdmin />
      </div>
    </div>
  </div>
  );
}
 
export default SettingAdmin;