import * as React from 'react';
import { TransactionTableAdmin } from '../../components';

interface TransactionAdminProps { }
 
const TransactionAdmin: React.FC<TransactionAdminProps> = () => {
  return (
    <div className="container-fluid py-4">
    <div className="row">
      <div className="col-md-12">
        <TransactionTableAdmin />
      </div>
    </div>
  </div>
  );
}
 
export default TransactionAdmin;