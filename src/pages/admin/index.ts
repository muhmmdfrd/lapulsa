import DashboardAdmin from './DashboardAdmin';
import ProductAdmin from './ProductAdmin';
import SettingAdmin from './SettingAdmin';

export {
  DashboardAdmin,
  ProductAdmin,
  SettingAdmin
}