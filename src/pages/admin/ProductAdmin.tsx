import * as React from 'react';
import { ProductTableAdmin } from '../../components';

interface ProductAdminProps { }
 
const ProductAdmin: React.FC<ProductAdminProps> = () => {
  return (
    <div className="container-fluid py-4">
      <div className="row">
        <div className="col-md-12">
          <ProductTableAdmin />
        </div>
      </div>
    </div>
  );
}
 
export default ProductAdmin;