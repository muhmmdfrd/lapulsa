import * as React from 'react';
import { useTranslation } from 'react-i18next';
import { DashCard } from '../../components';

interface DashboardAdminProps { }
 
const DashboardAdmin: React.FC<DashboardAdminProps> = () => {
  const { t } = useTranslation();
  
  return (
    <div className="container-fluid py-4">
      <div className="row">
        <DashCard name={t('dashboard.balance')} icon={'paid'} value={13891230} schema={'dark'} type={'finance'} />
        <DashCard name={t('dashboard.transaction_today')} icon={'person'} value={2300} schema={'primary'} />
        <DashCard name={t('dashboard.curse_today')} icon={'shopping_cart'} value={'$1 = Rp 14.226'} schema={'info'} />
        <DashCard name={t('dashboard.product')} icon={'summarize'} value={341} schema={'success'} />
      </div>
    </div>
  );
}
 
export default DashboardAdmin;