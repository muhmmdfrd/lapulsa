import * as React from 'react';
import { constant } from '../constants';

const ThemeContext = React.createContext<any>(null);

const initialState = {
  darkMode: false
}

const themeReducer = (state: any, action: any) => {
  switch (action.type) {
    case constant.light:
      return {
        ...state,
        darkMode: false
      }
    case constant.dark:
      return {
        ...state,
        darkMode: true
      }
    default:
      return state;
  }
}

const ThemeProvider: React.FC = ({ children }) => {
  const [state, dispatch] = React.useReducer(themeReducer, initialState);

  const value = {
    state: state,
    dispatch: dispatch
  };

  return (
    <ThemeContext.Provider value={value}>
      {children}
    </ThemeContext.Provider>
  );
}

export {
  ThemeContext,
  ThemeProvider
};