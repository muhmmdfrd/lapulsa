const toCurrency = (value: number, withLabel = false) => {
  return withLabel ?
    'Rp ' + value.toLocaleString().replaceAll(',', '.') :
    value.toLocaleString().replaceAll(',', '.');
}

const toDollar = (value: number) => {
  return '$ ' + value.toString().replaceAll('.', ',');
}

const toQuerystring = (url: string, obj: any): string => {
  let str = [];

  for (let p in obj) {
    if (obj.hasOwnProperty(p)) {
      str.push(encodeURIComponent(p) + '=' + encodeURIComponent(obj[p]));
    }
  }
  return `${url}?${str.join('&')}`;
}

const StringHelper = {
  toCurrency,
  toDollar,
  toQuerystring
}

export default StringHelper;