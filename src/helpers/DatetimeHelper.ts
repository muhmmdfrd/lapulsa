import moment from 'moment';

const now = () => {
  return moment();
}

const dateNow = (): string => {
  return now().date().toString();
}

const monthNow = (): string => {
  return now().month().toString();
}

const yearNow = (): string => {
  return now().year().toString();
}

const format = (value: any) => {
  return moment(value).format('DD-MM-yyyy HH:mm');
}

const DatetimeHelper = {
  dateNow,
  monthNow,
  yearNow,
  format
};

export default DatetimeHelper;