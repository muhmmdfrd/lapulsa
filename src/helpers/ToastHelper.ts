import toast from "react-hot-toast";

const options: any = {
  position: 'top-center',
  duration: 3000
};

const success = (message: string) => {
  toast.success(message, options);
};

const error = (message: string) => {
  toast.error(message, options);
}

const promise = (promise: Promise<any>, message: string, error: string, success: string) => {
  toast.promise(promise, {
    loading: message,
    error: error,
    success : success
  });
}

const ToastHelper = {
  success,
  error,
  promise
};

export default ToastHelper;