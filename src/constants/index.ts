export const constant = {
  // theme
  light: 'LIGHT',
  dark: 'DARK',
  // lang
  lang: {
    id: 'id',
    en: 'end'
  }
};