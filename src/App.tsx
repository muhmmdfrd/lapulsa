import * as React from 'react';
import { Toaster } from 'react-hot-toast';
import { HashRouter } from 'react-router-dom';
import { ThemeProvider } from './contexts';
import { IndexRoute } from './routes';
import './App.css';

const App: React.FC = () => {
  return (
    <>
      <Toaster />
      <ThemeProvider>
        <HashRouter>
          <IndexRoute />
        </HashRouter>
      </ThemeProvider>
    </>
  )
}

export default App;
