import IndexRoute from './IndexRoute';
import CustomerRoute from './CustomerRoute';
import AdminRoute from './AdminRoute';
import SuperRoute from './SuperRoute';

export {
  IndexRoute,
  CustomerRoute,
  AdminRoute,
  SuperRoute
};