import * as React from 'react';
import { Route, Routes } from 'react-router-dom';
import { IndexRouteType } from '../types/index-route.type';

interface IndexRouteProps {}

const IndexRoute: React.FC<IndexRouteProps> = () => {
  const Customer = React.lazy(() => import('../CustomerApp'));
  const Admin = React.lazy(() => import('../AdminApp'));
  const Super = React.lazy(() => import('../SuperAdminApp'));

  const routes: IndexRouteType[] = [
    {
      path: '/*',
      element: <Customer />,
    },
    {
      path: '04d072c0-1aeb-414c-bd68-b02351d96acb/*',
      element: <Admin />,
    },
    {
      path: '04d072c0-1aeb-414c-bd68-b02351d96acd/*',
      element: <Super />,
    },
  ];

  return (
    <Routes>
      {routes.map((v) => (
        <Route
          key={v.path}
          path={v.path}
          element={
            <React.Suspense fallback={<p>Loading...</p>}>
              {v.element}
            </React.Suspense>
          }
        />
      ))}
    </Routes>
  );
};

export default IndexRoute;
