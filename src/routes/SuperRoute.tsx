import * as React from 'react';
import { Route, Routes } from 'react-router-dom';
import { IndexRouteType } from '../types/index-route.type';

interface SuperRouteProps { }
 
const SuperRoute: React.FC<SuperRouteProps> = () => {
  const DashboardPage = React.lazy(() => import('./../pages/admin/DashboardAdmin'));
  const ProductPage = React.lazy(() => import('./../pages/admin/ProductAdmin'));
  const TransactionPage = React.lazy(() => import('./../pages/admin/TransactionAdmin'));
  const SettingPage = React.lazy(() => import('./../pages/admin/SettingAdmin'));

  const routes: IndexRouteType[] = [
    {
      path: 'dashboard',
      element: <DashboardPage />
    },
    {
      path: 'product',
      element: <ProductPage />
    },
    {
      path: 'transaction',
      element: <TransactionPage />
    },
    {
      path: 'settings',
      element: <SettingPage />
    }
  ];

  return (
    <Routes>
      {routes.map((v) => <Route key={v.path} path={v.path} element={<React.Suspense fallback={<p>Loading...</p>}>{v.element}</React.Suspense>} />)}
    </Routes>
  );
}
 
export default SuperRoute;