import * as React from 'react';
import { Route, Routes } from 'react-router-dom';
import { IndexRouteType } from '../types/index-route.type';

interface AdminRouteProps {}

const AdminRoute: React.FC<AdminRouteProps> = () => {
  const LoginPage = React.lazy(() => import('./../pages/LoginPage'));

  const routes: IndexRouteType[] = [
    {
      path: 'login',
      element: <LoginPage />,
    },
  ];

  return (
    <Routes>
      {routes.map((v) => (
        <Route
          key={v.path}
          path={v.path}
          element={
            <React.Suspense fallback={<p>Loading...</p>}>
              {v.element}
            </React.Suspense>
          }
        />
      ))}
    </Routes>
  );
};

export default AdminRoute;
