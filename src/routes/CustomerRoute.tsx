import * as React from 'react';
import { Route, Routes } from 'react-router-dom';
import { IndexRouteType } from '../types/index-route.type';

interface CustomerRouteProps {}

const CustomerRoute: React.FC<CustomerRouteProps> = () => {
  const DashboardPage = React.lazy(() => import('./../pages/DashboardPage'));
  const ProductPage = React.lazy(() => import('./../pages/ProductPage'));
  const TransactionPage = React.lazy(
    () => import('./../pages/TransactionPage')
  );
  const TransactionDetailPage = React.lazy(
    () => import('./../pages/TransactionDetailPage')
  );

  const routes: IndexRouteType[] = [
    {
      path: '/',
      element: <DashboardPage />,
    },
    {
      path: 'product',
      element: <ProductPage />,
    },
    {
      path: 'transaction',
      element: <TransactionPage />,
    },
    {
      path: 'transaction/:id',
      element: <TransactionDetailPage />,
    },
  ];

  return (
    <Routes>
      {routes.map((v) => (
        <Route
          index={false}
          key={v.path}
          path={v.path}
          element={
            <React.Suspense fallback={<p>Loading...</p>}>
              {v.element}
            </React.Suspense>
          }
        />
      ))}
    </Routes>
  );
};

export default CustomerRoute;
