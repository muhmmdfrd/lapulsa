import * as React from 'react';
import { Navbar, Setting, Sidebar } from './components';
import { CustomerRoute } from './routes';

const CustomerApp: React.FC = () => {
  return (
    <>
      <Sidebar />
      <main className="main-content position-relative max-height-vh-100 h-100 border-radius-lg ">
        <Navbar />
        <CustomerRoute />
        <Setting />
      </main>
    </>
  )
}

export default CustomerApp;
