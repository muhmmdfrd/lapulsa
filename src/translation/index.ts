import i18n from 'i18next';
import { initReactI18next } from 'react-i18next';
import { constant } from '../constants';
import en from './en.json';
import id from './id.json';

i18n
  .use(initReactI18next)
  .init({
    resources: {
      en: {
        translation: en
      },
      id: {
        translation: id
      }
    },
    lng: constant.lang.id, // change this line for change default language
    interpolation: {
      escapeValue: false
    }
  });