import * as React from 'react';
import { Navbar, Setting, SidebarAdmin } from './components';
import { SuperRoute } from './routes';

interface SuperAdminAppProps { }
 
const SuperAdminApp: React.FC<SuperAdminAppProps> = () => {
  return (
    <>
      <SidebarAdmin />
      <main className="main-content position-relative max-height-vh-100 h-100 border-radius-lg ">
        <Navbar />
        <SuperRoute />
        <Setting />
      </main>
    </>
  );
}
 
export default SuperAdminApp;